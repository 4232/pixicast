Pixicast
========

Speedcast of the work of the day

~~~
pixicast 1
~~~
> A screenshot for second

~~~
cd ~/Videos/pixicast-*/
pixicast audio.ogg
~~~
> Generate video with a song

Examples
--------

* [4232.c](http://4232.cf/blog/)

Helpme ♥
-------

* Bazza is creating [4232](http://4232.cf/donaciones) a Sci-Fi Animation made with Free Software
* **Bitcoin:** 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs
