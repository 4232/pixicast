Pixicast
========

[English](README.en.md)

Speedcast del trabajo del día

~~~
pixicast 1
~~~
> Toma una captura cada un segundo de nuestra pantalla

~~~
cd ~/Videos/pixicast-*/
pixicast audio.ogg
~~~
> Genera el video en webm de las capturas realizadas, lo sube a archive.org con s3upload y lo publica en el blog con wp-cli

Ejemplos
--------

* [4232.cf](http://4232.cf/blog/)

Ayuda ♥
-------

* Ayudame a terminar el [4232](http://4232.cf/donaciones) un corto de animación realizado con software libre.
* **Bitcoin:** 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs
